package places;

import person.Person;

public class Bar {
    public void visit(Person p) {
        if (p.getAge() < 18) {
            System.out.println("Too young!");
        } else {
            System.out.println("Welcome");
            if (p.getMoney() > 0) {
                p.setMoney(p.getMoney() - 10);
                p.setDrunk(true);

            }

        }
    }
}
