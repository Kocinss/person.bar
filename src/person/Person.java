package person;

public class Person {
    private int age;
    private boolean drunk;
    private double money;

    public Person(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setDrunk(boolean drunk) {
        this.drunk = drunk;
    }

    public boolean isDrunk(){
    return drunk ;
    };

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", drunk=" + drunk +
                ", money=" + money +
                '}';
    }
}
